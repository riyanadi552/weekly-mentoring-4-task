const form = document.getElementById("item-form");
const input = document.getElementById("item-title");
const itemLane = document.getElementById("item-lane");
const droppables = document.querySelectorAll(".card");

function createDragListeners(item) {
  item.addEventListener("dragstart", () => {
    item.classList.add("is-dragging");
  });

  item.addEventListener("dragend", () => {
    item.classList.remove("is-dragging");
  });
}

const insertAboveitem = (zone, mouseY) => {
  const els = zone.querySelectorAll(".item:not(.is-dragging)");

  let closestitem = null;
  let closestOffset = Number.NEGATIVE_INFINITY;

  els.forEach((item) => {
    const { top } = item.getBoundingClientRect();

    const offset = mouseY - top;

    if (offset < 0 && offset > closestOffset) {
      closestOffset = offset;
      closestitem = item;
    }
  });

  return closestitem;
};

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const title = document.getElementById("item-title").value;
  const desc = document.getElementById("item-desc").value;

  if (!title || !desc) {
    alert("Tolong isi kedua kolom yang tersedia!");
    return;
  }

  const newItem = document.createElement("div");
  newItem.classList.add("item");
  newItem.setAttribute("draggable", "true");

  const titleElemt = document.createElement("h3");
  titleElemt.innerText = title;

  const descElemt = document.createElement("p");
  descElemt.innerText = desc;

  newItem.appendChild(titleElemt);
  newItem.appendChild(descElemt);

  const deleteButton = createDeleteButton(newItem);
  newItem.appendChild(deleteButton);

  itemLane.appendChild(newItem);

  document.getElementById("item-title").value = "";
  document.getElementById("item-desc").value = "";

  createDragListeners(newItem);
});

function createDeleteButton(item) {
  const deleteButton = document.createElement("button");
  deleteButton.innerText = "Delete";
  deleteButton.classList.add("delete-button");
  deleteButton.style.marginTop = "10px"; 
  
  deleteButton.addEventListener("click", () => {
    item.remove();
  });

  return deleteButton;
}

droppables.forEach((zone) => {
  zone.addEventListener("dragover", (e) => {
    e.preventDefault();

    const bottomitem = insertAboveitem(zone, e.clientY);
    const curitem = document.querySelector(".is-dragging");

    if (!bottomitem) {
      zone.appendChild(curitem);
    } else {
      zone.insertBefore(curitem, bottomitem);
    }
  });
});

const existingitems = document.querySelectorAll(".item");
existingitems.forEach((item) => {
  createDragListeners(item);
  const deleteButton = createDeleteButton(item);
  item.appendChild(deleteButton);
});
